# ci-tests
Scripts helpful for testing a Drupal site on a Continuous-Integration server, or locally.

This package provides example tests:

     behat/behat-run.sh      Behavior Driven Development
     code-fixer.sh           PHP Code Beautifier and Fixer
     code-sniffer.sh         PHP CodeSniffer
     pa11y/pa11y-review.sh   Pa11y Accessibility
     phpunit.sh              PHPUnit
     security-review.sh      Drupal 7 Security Review module



##Installation

```
composer require mediacurrent/ci-tests
```
     
It may be necessary to define the package in the repositories section of composer.json:

```
"repositories": [
    {
        "type": "vcs",
        "url": "git@bitbucket.org:mediacurrent/ci-tests.git"
    }
],
```

##Credit

Thanks to https://drupal.org/project/doobie for example behat configuration.
